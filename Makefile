CXX = g++ -g -fPIC -std=c++14 #-DDEBUG
NETLIBS= -lnsl

all: myhttp

myhttp: myhttp.o
	$(CXX) -o $@ $@.o $(NETLIBS) -lpthread

clean:
	rm -f *.o myhttp *~ *#
